#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initUi();
    initDB();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initUi()
{
    ui->stackedWidget->setCurrentIndex(2);
    m_userDialog = new UserDialog;
    //ui->textEdit->setText("PDF 변환 영역");
}

void MainWindow::initDB()
{
    m_db = QSqlDatabase::addDatabase("QSQLITE","members");//데이터베이스 종류설정

    m_db.setDatabaseName("members.db"); //데이터베이스 파일이름설정
    qDebug() << m_db.open();
    //qDebug() << __FUNCTION__ <<  m_db.transaction();

    qDebug() << m_db.tables();
    m_userModel = new UserTableModel(m_db);
    m_userDialog->setDatabase(m_db);

}

void MainWindow::on_loginBtn_clicked()
{
    emit test();
    QSqlQuery query(m_db);
    //QString tmp = QString("SELECT * FROM member");

    QString tmp = QString("SELECT Grade FROM member WHERE ID='%1' and PW='%2'") \
            .arg(ui->loginID->text()).arg(ui->loginPW->text());
    query.prepare(tmp);
    qDebug() << query.lastQuery();

    if(!query.exec()){
        qDebug() << query.lastError().text();
        return;
    }

    if(query.next())
    {
        int grade = query.value(0).toInt();
        if(grade == 1)
        {
            qDebug() << "Admin";
            ui->stackedWidget->setCurrentIndex(1);
            m_userModel->setTable();
            ui->tableView->setModel(m_userModel);

        }else
        {
            ui->stackedWidget->setCurrentIndex(2);
            qDebug() << "Normal";
        }
    }else
    {
        qDebug() << "login fail";
    }

}

void MainWindow::on_AdminLogoutBtn_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->loginID->clear();
    ui->loginPW->clear();
}

void MainWindow::on_AdminBtn_clicked()
{
    m_userDialog->setUserID(ui->loginID->text());
    m_userDialog->exec();
    m_userModel->setTable();
}

void MainWindow::on_AddUserBtn_clicked()
{
    m_userDialog->setUserID();
    m_userDialog->exec();
    m_userModel->setTable();
}

void MainWindow::on_ModifyUserBtn_clicked()
{
    m_userDialog->setUserID(m_userModel->getIDbyRow(ui->tableView->currentIndex().row()));
    m_userDialog->exec();
    m_userModel->setTable();
}

void MainWindow::on_DelUserBtn_clicked()
{
    QSqlQuery query(m_db);
    QString tmp = QString("DELETE FROM member WHERE ID='%1'") \
            .arg(m_userModel->getIDbyRow(ui->tableView->currentIndex().row()));
    query.prepare(tmp);
    qDebug() << query.lastQuery();

    if(!query.exec()){
        qDebug() << query.lastError().text();
        return;
    }
    m_userModel->setTable();
}

void MainWindow::on_btnFilePath_clicked()
{
    ui->edPDFPath->setText(QFileDialog::getOpenFileName(this, tr("Open PDF"), NULL, tr("PDF File (*.pdf)")).replace("/","\\"));
}

void MainWindow::on_btnConvert_clicked()
{
    QString path = ui->edPDFPath->text();

    emit convertPDF(path);
}
