#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QMutex>
#include <QProcess>
#include <QStringList>

class BackEnd : public QObject
{
    Q_OBJECT
public:
    explicit BackEnd(QObject *parent = nullptr);
    ~BackEnd();


public slots:
    void initThread();
    void safeQuit();
    void testExcel();
    void onConvertPDF(QString path);

private slots:
    void	onProcessError();
    void	onProcessOutput();
    void    onProcessFinished(int, QProcess::ExitStatus);


private:
    QMutex                  m_mutex;
    QProcess *              m_process;
    QString                 m_readData;
    QList<QStringList>      m_dataTable;
    QString                 m_filePath;

    void                    exportExcel();

};

#endif // BACKEND_H
