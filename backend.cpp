#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"

#include "backend.h"
#include <QDebug>
#include <QDateTime>
#include <QDir>

#include <QRegularExpression>
#include <QRegularExpressionMatch>
/*
void writeBorderStyleCell(Document &xlsx, const QString &cell, const QString &text, Format::BorderStyle bs)
{
   Format format;
   format.setBorderStyle(bs);
   xlsx.write(cell, text, format);
}
*/

using namespace QXlsx;

BackEnd::BackEnd(QObject *parent) : QObject(parent)
{

}
BackEnd::~BackEnd()
{
    qDebug() << __FUNCTION__;
}
void BackEnd::initThread()
{
    qDebug() << __FUNCTION__;

    m_process = new QProcess;
    m_process->setProgram(QDir::currentPath()+"/pdftotext.exe");

    connect(m_process,SIGNAL(readyReadStandardError()),this,SLOT(onProcessError()));
    connect(m_process,SIGNAL(readyReadStandardOutput()),this,SLOT(onProcessOutput()));
    connect(m_process,SIGNAL(finished(int, QProcess::ExitStatus)),this,SLOT(onProcessFinished(int, QProcess::ExitStatus)));

}
void BackEnd::safeQuit()
{
    m_mutex.lock();
    //while (m_dbActive);
    //m_isQuit = true;
    m_mutex.unlock();
    qDebug() << __FUNCTION__;
}

void BackEnd::onConvertPDF(QString path)
{
    qDebug() << __FUNCTION__ << QDir::currentPath();

    m_filePath = path;
    m_dataTable.clear();
    m_readData.clear();
    m_process->setArguments({"-layout","-simple","-enc","UTF-8",path, "-"});

    m_process->setArguments({"-raw","-enc","UTF-8",path, "-"});
    m_process->start();

}

void BackEnd::testExcel()
{
    qDebug() << __FUNCTION__ << QDateTime::currentDateTime();
    QXlsx::Document xlsx;
    xlsx.write("A1", "Hello Qt!"); // write "Hello Qt!" to cell(A,1). it's shared string.

    Format format;
    format.setBorderStyle(Format::BorderThin);
    xlsx.write("C3", "한글", format);

    xlsx.cellAt(1,1)->format().setBorderStyle(Format::BorderThin);
    //writeBorderStyleCell(xlsx, "B21", "BorderDouble", Format::BorderDouble);
    qDebug() << xlsx.saveAs("Tests.xlsx"); // save the document as 'Test.xlsx'
    qDebug() << __FUNCTION__ << QDateTime::currentDateTime();

}

void BackEnd::onProcessError()
{
    qDebug() << m_process->readAllStandardError();
}
void BackEnd::onProcessOutput()
{
    QString tmp = m_process->readAllStandardOutput();
    m_readData.append(tmp);
}
void BackEnd::onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    if(exitStatus != QProcess::NormalExit)
        return;

    qDebug() << __FUNCTION__ << m_readData;

    m_readData.replace("\r\n","\n");
    m_readData.replace("\f","");

    m_readData.replace("\n\n","\n");
    m_readData.replace(QRegularExpression(" +")," ");


    QStringList fullRows = m_readData.split("\n",Qt::SkipEmptyParts);
    for(int i = 0 ; i < fullRows.count() ; i++ )
    {
        QStringList tmp = fullRows.at(i).split(" ",Qt::SkipEmptyParts);
        m_dataTable.append(tmp);

    }
    qDebug() << m_dataTable;

    qDebug() << m_readData;
    exportExcel();

}

void BackEnd::exportExcel()
{
    QXlsx::Document xlsx;

    Format format;
    format.setBorderStyle(Format::BorderThin);
    format.setHorizontalAlignment(Format::AlignHCenter);
    format.setVerticalAlignment(Format::AlignVCenter);


    int row = 2;

    for(int i = 0 ; i < m_dataTable.count() ; i++)
    {
        if(m_dataTable.at(i).count() == 1)
        {
            xlsx.write(2,2,m_dataTable.at(i).at(0),format);
            continue;
        }

        for(int j = 0 ; j < m_dataTable.at(i).count() ; j++)
        {
            xlsx.write(row,j+3,m_dataTable.at(i).at(j),format);
        }
        row++;
    }

    xlsx.mergeCells("B2:B6",format);


    m_filePath.replace(".pdf",".xlsx");

    qDebug() << xlsx.saveAs(m_filePath); // save the document as 'Test.xlsx'
}
