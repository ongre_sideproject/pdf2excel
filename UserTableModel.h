#ifndef USERTABLEMODEL_H
#define USERTABLEMODEL_H

#include <QObject>
#include <QSqlQueryModel>
#include <QIdentityProxyModel>


class UserTableModel : public QIdentityProxyModel
{
    Q_OBJECT
public:
    UserTableModel(QSqlDatabase db = QSqlDatabase());

    QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;
    bool setTable();
    QString getIDbyRow(int index);

private:
    QSqlQueryModel *    m_tableModel;
    QSqlDatabase        m_db;

};

#endif // USERTABLEMODEL_H
