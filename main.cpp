#include "mainwindow.h"

#include <QApplication>
#include <QThread>
#include "BackEnd.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    BackEnd backend;

    QObject::connect(&w,&MainWindow::test,&backend,&BackEnd::testExcel);
    QObject::connect(&w,&MainWindow::convertPDF,&backend,&BackEnd::onConvertPDF);




    QThread * thread = new QThread;
    backend.moveToThread(thread);
    QObject::connect(thread,SIGNAL(started()),&backend,SLOT(initThread()));
    //QObject::connect(&w, &MainWindow::safeQuit, &backend, &BackEnd::safeQuit,Qt::DirectConnection);


    //QObject::connect(&w, &MainWindow::deleteLater, &backend, &BackEnd::safeQuit,Qt::DirectConnection);
    //QObject::connect(thread, &QThread::deleteLater, &backend, &BackEnd::safeQuit,Qt::DirectConnection);

    thread->start();

    w.show();
    return a.exec();
}
