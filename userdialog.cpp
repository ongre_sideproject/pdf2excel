#include "userdialog.h"
#include "ui_userdialog.h"
#include <QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include <QSqlError>

UserDialog::UserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserDialog)
{
    ui->setupUi(this);
}

UserDialog::~UserDialog()
{
    delete ui;
}

void UserDialog::setDatabase(QSqlDatabase db)
{
    m_db = db;
}
void UserDialog::setUserID(QString id)
{
    m_userID = id;

    if(m_userID.isEmpty())
    {
        ui->ID->clear();
        ui->Name->clear();
    }else
    {
        ui->ID->setText(m_userID);
        QSqlQuery query(m_db);
        QString tmp = QString("SELECT NAME from member WHERE ID='%1'").arg(m_userID);
        query.prepare(tmp);

        if(!query.exec()){
            qDebug() << query.lastError().text();
            return;
        }

        if(query.next())
        {
            ui->Name->setText(query.value(0).toString());

        }else
        {
            qDebug() << "Error";
            //return;
        }

    }
    ui->Password_1->clear();
    ui->Password_2->clear();

}

void UserDialog::on_OkBtn_clicked()
{
    QSqlQuery query(m_db);

    QString tmp;

    if(ui->Password_1->text().isEmpty())
    {
        QMessageBox(QMessageBox::Critical, "Error", "password", QMessageBox::Ok).exec();
        return;
    }

    if(ui->Password_1->text().compare(ui->Password_2->text()))
    {
        QMessageBox(QMessageBox::Critical, "Error", "password", QMessageBox::Ok).exec();
        return;
    }

    if(m_userID.isEmpty())
    {
        tmp = QString("INSERT INTO member (Grade,ID,PW,NAME) VALUES (2,'%1','%2','%3')") \
                .arg(ui->ID->text()).arg(ui->Password_1->text()).arg(ui->Name->text());
    }else
    {
        tmp = QString("UPDATE member SET PW='%1',NAME='%2' WHERE ID='%3'") \
                .arg(ui->Password_1->text()).arg(ui->Name->text()).arg(m_userID);

    }
    qDebug() << tmp;
    query.prepare(tmp);
    if(!query.exec()){
        QMessageBox(QMessageBox::Critical, "Error", "Fail", QMessageBox::Ok).exec();
        qWarning() << __PRETTY_FUNCTION__ << "qurey error";
        return;
    }

    this->close();
}

void UserDialog::on_CancelBtn_clicked()
{
    this->close();
}
