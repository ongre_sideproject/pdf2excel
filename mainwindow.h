#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>

#include "UserTableModel.h"
#include "userdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
signals:
    void test();
    void convertPDF(QString path);

private slots:
    void on_loginBtn_clicked();

    void on_AdminLogoutBtn_clicked();

    void on_AdminBtn_clicked();

    void on_AddUserBtn_clicked();

    void on_ModifyUserBtn_clicked();

    void on_DelUserBtn_clicked();

    void on_btnFilePath_clicked();

    void on_btnConvert_clicked();

private:
    Ui::MainWindow *ui;
    QSqlDatabase        m_db;
    UserTableModel      *m_userModel;

    UserDialog          *m_userDialog;

    void initDB();
    void initUi();
};
#endif // MAINWINDOW_H
