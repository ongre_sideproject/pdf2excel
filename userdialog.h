#ifndef USERDIALOG_H
#define USERDIALOG_H

#include <QDialog>
#include <QSqlDatabase>

namespace Ui {
class UserDialog;
}

class UserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UserDialog(QWidget *parent = nullptr);
    ~UserDialog();

    void setDatabase(QSqlDatabase db);
    void setUserID(QString id = "");

private slots:
    void on_OkBtn_clicked();

    void on_CancelBtn_clicked();

private:
    Ui::UserDialog *ui;

    QSqlDatabase        m_db;
    QString             m_userID;

};

#endif // USERDIALOG_H
