#include "UserTableModel.h"
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QRegExp>

UserTableModel::UserTableModel(QSqlDatabase db)
{
    m_tableModel=new QSqlQueryModel;
    m_db = db;
}

QVariant UserTableModel::data(const QModelIndex &idx, int role) const
{
    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;

    else if(role == Qt::DisplayRole && (idx.column() == 1))
    {
        QString tmp = QIdentityProxyModel::data(idx, role).toString();
        tmp.replace(QRegExp("."),"*");
        return tmp;
    }

    return QIdentityProxyModel::data(idx, role);
}
bool UserTableModel::setTable()
{
    QString query = QString("SELECT ID,PW,NAME FROM member");

    m_tableModel->setQuery(query,m_db);
    QStringList title = {"ID","Password","이름"};


    for(int i = 0 ; i < m_tableModel->columnCount() ; i++)
    {
        m_tableModel->setHeaderData(i, Qt::Horizontal, title.at(i));
    }

    this->setSourceModel(m_tableModel);
    return true;
}

QString UserTableModel::getIDbyRow(int index)
{
    return  m_tableModel->data(m_tableModel->index(index,0)).toString();
}
